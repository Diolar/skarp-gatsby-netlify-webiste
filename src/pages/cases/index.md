---
templateKey: 'cases-page'
path: /cases
cases:
  - title: 'Case 1'
    subtitle: 'Subtile'
    stack: 'our stack'
    description: Perfect for the drinker who likes to enjoy 1-2 cups per day.
    image: ''
    device: 'iphone'
---
