---
templateKey: index-page
title: Skarp
image: /img/home-jumbotron.jpg
background: 'linear-gradient(-65deg, #0044FF 0%, #007BFF 100%)'
firstPitch: "/about/firstPitch.md"
secondPitch: "/about/secondPitch.md"
tagline: We're up for a challenge, let's create something amazing together
devStack:
  heading: DevStack
  description: Concept, Design & Programming
  stacks:
    - title: front-end
      technologies:
        - title: React/Angular
        - title: React Native
        - title: Progressive Web App/Instant App
      comment: Usable, beautiful, progressive responsive and native UI's on mobile, tablet, laptop and desktop
---
