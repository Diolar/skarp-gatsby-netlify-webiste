import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Layout from '../components/Layout'


export const ProductPageTemplate = ({ cases }) => (
  <section id="cases" className="section cases z-1">
    {cases && cases.map(({ title, subtitle, description, stack, image, device }) =>
      <div className="case bg-case">
        <div className="flex-grid">
          <div className="flex-row full-height">
            <div className="flex-item-50 full-height">
              <div className="flex-align">
                <div className={`case__device case__device--${device} z-2`}>
                  {device === "iphone" && (
                    <div className="iphone-padding-hack">
                      <svg className="case__svg" viewBox="0 0 416 848" version="1.1" xmlns="http://www.w3.org/2000/svg"
                           xmlnsXlink="http://www.w3.org/1999/xlink">
                        <title>iphone</title>
                        <defs>
                          <rect id="path-1" x="0" y="0" width="416" height="848" rx="64"></rect>
                          <filter x="-1.4%" y="-0.7%" width="102.9%" height="101.4%" filterUnits="objectBoundingBox"
                                  id="filter-2">
                            <feGaussianBlur stdDeviation="3" in="SourceAlpha" result="shadowBlurInner1"></feGaussianBlur>
                            <feOffset dx="0" dy="-6" in="shadowBlurInner1" result="shadowOffsetInner1"></feOffset>
                            <feComposite in="shadowOffsetInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1"
                                         result="shadowInnerInner1"></feComposite>
                            <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" type="matrix"
                                           in="shadowInnerInner1"></feColorMatrix>
                          </filter>
                          <linearGradient x1="0%" y1="0%" x2="100%" y2="100%" id="linearGradient-3">
                            <stop stop-color="#FAFCFE" offset="0%"></stop>
                            <stop stop-color="#F2F4F6" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient-4">
                            <stop stop-color="#E8EAEC" offset="0%"></stop>
                            <stop stop-color="#F4F6F8" offset="49.798943%"></stop>
                            <stop stop-color="#F6F8FA" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="83.9576666%" y1="1.96998351%" x2="14.9512656%" y2="96.0345747%"
                                          id="linearGradient-5">
                            <stop stop-color="#BABCBE" offset="0%"></stop>
                            <stop stop-color="#CACCCE" offset="26.7759563%"></stop>
                            <stop stop-color="#EAECEE" offset="51.5030243%"></stop>
                            <stop stop-color="#CACCCE" offset="85.1595538%"></stop>
                            <stop stop-color="#9A9C9E" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient-6">
                            <stop stop-color="#DADCDE" offset="0%"></stop>
                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                          </linearGradient>
                          <circle id="path-7" cx="208" cy="792" r="28"></circle>
                          <linearGradient x1="100%" y1="50%" x2="0%" y2="50%" id="linearGradient-8">
                            <stop stop-color="#BABCBE" offset="0%"></stop>
                            <stop stop-color="#CED0D2" offset="25%"></stop>
                            <stop stop-color="#E2E4E6" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="-1.11022302e-14%" y1="50%" x2="100%" y2="50%" id="linearGradient-9">
                            <stop stop-color="#BABCBE" offset="0%"></stop>
                            <stop stop-color="#CED0D2" offset="25%"></stop>
                            <stop stop-color="#E2E4E6" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient-10">
                            <stop stop-color="#0033CC" offset="0%"></stop>
                            <stop stop-color="#362586" offset="100%"></stop>
                          </linearGradient>
                          <rect id="path-11" x="0" y="0" width="352" height="640"></rect>
                          <pattern id="pattern-12" width="200" height="21" x="-200" y="-21" patternUnits="userSpaceOnUse">
                            <use xlinkHref="#image-13"></use>
                          </pattern>
                          <image id="image-13" width="200" height="21" xlinkHref={image}></image>
                          <linearGradient x1="0%" y1="0%" x2="100%" y2="100%" id="linearGradient-14">
                            <stop stop-color="#FFFFFF" stop-opacity="0.7" offset="0%"></stop>
                            <stop stop-color="#000000" stop-opacity="0.1" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="0%" y1="0%" x2="100%" y2="100%" id="linearGradient-15">
                            <stop stop-color="#555555" offset="0%"></stop>
                            <stop stop-color="#000000" offset="100%"></stop>
                          </linearGradient>
                          <rect id="path-16" x="32" y="104" width="352" height="640"></rect>
                          <linearGradient x1="50%" y1="0%" x2="50%" y2="99.4300064%" id="linearGradient-17">
                            <stop stop-color="#CACCCE" offset="0%"></stop>
                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                          </linearGradient>
                          <rect id="path-18" x="172" y="54" width="72" height="5" rx="2.5"></rect>
                          <radialGradient cx="50%" cy="59.2669947%" fx="50%" fy="59.2669947%" r="53.8948642%"
                                          id="radialGradient-19">
                            <stop stop-color="#0000FF" offset="0%"></stop>
                            <stop stop-color="#000000" offset="100%"></stop>
                          </radialGradient>
                          <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient-20">
                            <stop stop-color="#CACCCE" offset="0%"></stop>
                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                          </linearGradient>
                          <circle id="path-21" cx="144" cy="56" r="6"></circle>
                          <linearGradient x1="50%" y1="0%" x2="50%" y2="99.4300064%" id="linearGradient-22">
                            <stop stop-color="#CECECE" offset="0%"></stop>
                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                          </linearGradient>
                          <circle id="path-23" cx="208" cy="32" r="4"></circle>
                        </defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g id="iphone">
                            <g id="body">
                              <use fill="#EAECEE" fill-rule="evenodd" xlinkHref="#path-1"></use>
                              <use fill="black" fill-opacity="1" filter="url(#filter-2)" xlinkHref="#path-1"></use>
                            </g>
                            <rect id="glass" fill="url(#linearGradient-3)" x="8" y="8" width="400" height="832"
                                  rx="56"></rect>
                            <g id="button">
                              <use fill="url(#linearGradient-4)" fill-rule="evenodd" xlinkHref="#path-7"></use>
                              <circle stroke="url(#linearGradient-5)" stroke-width="4" cx="208" cy="792" r="30"></circle>
                              <circle stroke="url(#linearGradient-6)" stroke-width="2" cx="208" cy="792" r="27"></circle>
                            </g>
                            <g id="antenna" transform="translate(0.000000, 69.000000)">
                              <rect id="bottom-right" fill="url(#linearGradient-8)" x="408" y="697" width="8"
                                    height="13"></rect>
                              <rect id="bottom-left" fill="url(#linearGradient-9)" x="0" y="697" width="8"
                                    height="13"></rect>
                              <rect id="top-right" fill="url(#linearGradient-8)" x="408" y="0" width="8" height="13"></rect>
                              <rect id="top-left" fill="url(#linearGradient-9)" x="0" y="0" width="8" height="13"></rect>
                            </g>
                            <g id="june" transform="translate(32.000000, 104.000000)">
                              <g id="background">
                                <use fill="url(#linearGradient-10)" xlinkHref="#path-11"></use>
                                <use fill="url(#pattern-12)" xlinkHref="#path-11"></use>
                              </g>
                              <g id="load" transform="translate(130.000000, 536.000000)">
                                <circle id="Oval-3" stroke="#FFFFFF" stroke-width="1.83333333" cx="46" cy="46"
                                        r="21.0833333"></circle>
                                <path
                                  d="M46,16 C44.8954305,16 44,15.1045695 44,14 C44,12.8954305 44.8954305,12 46,12 C47.1045695,12 48,12.8954305 48,14 C48,15.1045695 47.1045695,16 46,16 Z M53.7645714,17.0222252 C52.6976391,16.7363416 52.0644742,15.6396677 52.3503578,14.5727355 C52.6362414,13.5058033 53.7329153,12.8726383 54.7998475,13.1585219 C55.8667797,13.4444055 56.4999447,14.5410794 56.2140611,15.6080116 C55.9281775,16.6749439 54.8315036,17.3081088 53.7645714,17.0222252 Z M61,20.0192379 C60.0434148,19.4669531 59.7156644,18.2437723 60.2679492,17.2871871 C60.8202339,16.3306018 62.0434148,16.0028515 63,16.5551363 C63.9565852,17.107421 64.2843356,18.3306018 63.7320508,19.2871871 C63.1797661,20.2437723 61.9565852,20.5715226 61,20.0192379 Z M67.2132034,24.7867966 C66.4321549,24.005748 66.4321549,22.739418 67.2132034,21.9583694 C67.994252,21.1773209 69.260582,21.1773209 70.0416306,21.9583694 C70.8226791,22.739418 70.8226791,24.005748 70.0416306,24.7867966 C69.260582,25.5678451 67.994252,25.5678451 67.2132034,24.7867966 Z M71.9807621,31 C71.4284774,30.0434148 71.7562277,28.8202339 72.7128129,28.2679492 C73.6693982,27.7156644 74.892579,28.0434148 75.4448637,29 C75.9971485,29.9565852 75.6693982,31.1797661 74.7128129,31.7320508 C73.7562277,32.2843356 72.5330469,31.9565852 71.9807621,31 Z M74.9777748,38.2354286 C74.6918912,37.1684964 75.3250561,36.0718225 76.3919884,35.7859389 C77.4589206,35.5000553 78.5555945,36.1332203 78.8414781,37.2001525 C79.1273617,38.2670847 78.4941967,39.3637586 77.4272645,39.6496422 C76.3603323,39.9355258 75.2636584,39.3023609 74.9777748,38.2354286 Z M76,46 C76,44.8954305 76.8954305,44 78,44 C79.1045695,44 80,44.8954305 80,46 C80,47.1045695 79.1045695,48 78,48 C76.8954305,48 76,47.1045695 76,46 Z M74.9777748,53.7645714 C75.2636584,52.6976391 76.3603323,52.0644742 77.4272645,52.3503578 C78.4941967,52.6362414 79.1273617,53.7329153 78.8414781,54.7998475 C78.5555945,55.8667797 77.4589206,56.4999447 76.3919884,56.2140611 C75.3250561,55.9281775 74.6918912,54.8315036 74.9777748,53.7645714 Z M71.9807621,61 C72.5330469,60.0434148 73.7562277,59.7156644 74.7128129,60.2679492 C75.6693982,60.8202339 75.9971485,62.0434148 75.4448637,63 C74.892579,63.9565852 73.6693982,64.2843356 72.7128129,63.7320508 C71.7562277,63.1797661 71.4284774,61.9565852 71.9807621,61 Z M67.2132034,67.2132034 C67.994252,66.4321549 69.260582,66.4321549 70.0416306,67.2132034 C70.8226791,67.994252 70.8226791,69.260582 70.0416306,70.0416306 C69.260582,70.8226791 67.994252,70.8226791 67.2132034,70.0416306 C66.4321549,69.260582 66.4321549,67.994252 67.2132034,67.2132034 Z M61,71.9807621 C61.9565852,71.4284774 63.1797661,71.7562277 63.7320508,72.7128129 C64.2843356,73.6693982 63.9565852,74.892579 63,75.4448637 C62.0434148,75.9971485 60.8202339,75.6693982 60.2679492,74.7128129 C59.7156644,73.7562277 60.0434148,72.5330469 61,71.9807621 Z M53.7645714,74.9777748 C54.8315036,74.6918912 55.9281775,75.3250561 56.2140611,76.3919884 C56.4999447,77.4589206 55.8667797,78.5555945 54.7998475,78.8414781 C53.7329153,79.1273617 52.6362414,78.4941967 52.3503578,77.4272645 C52.0644742,76.3603323 52.6976391,75.2636584 53.7645714,74.9777748 Z M46,76 C47.1045695,76 48,76.8954305 48,78 C48,79.1045695 47.1045695,80 46,80 C44.8954305,80 44,79.1045695 44,78 C44,76.8954305 44.8954305,76 46,76 Z M38.2354286,74.9777748 C39.3023609,75.2636584 39.9355258,76.3603323 39.6496422,77.4272645 C39.3637586,78.4941967 38.2670847,79.1273617 37.2001525,78.8414781 C36.1332203,78.5555945 35.5000553,77.4589206 35.7859389,76.3919884 C36.0718225,75.3250561 37.1684964,74.6918912 38.2354286,74.9777748 Z M31,71.9807621 C31.9565852,72.5330469 32.2843356,73.7562277 31.7320508,74.7128129 C31.1797661,75.6693982 29.9565852,75.9971485 29,75.4448637 C28.0434148,74.892579 27.7156644,73.6693982 28.2679492,72.7128129 C28.8202339,71.7562277 30.0434148,71.4284774 31,71.9807621 Z M24.7867966,67.2132034 C25.5678451,67.994252 25.5678451,69.260582 24.7867966,70.0416306 C24.005748,70.8226791 22.739418,70.8226791 21.9583694,70.0416306 C21.1773209,69.260582 21.1773209,67.994252 21.9583694,67.2132034 C22.739418,66.4321549 24.005748,66.4321549 24.7867966,67.2132034 Z M20.0192379,61 C20.5715226,61.9565852 20.2437723,63.1797661 19.2871871,63.7320508 C18.3306018,64.2843356 17.107421,63.9565852 16.5551363,63 C16.0028515,62.0434148 16.3306018,60.8202339 17.2871871,60.2679492 C18.2437723,59.7156644 19.4669531,60.0434148 20.0192379,61 Z M17.0222252,53.7645714 C17.3081088,54.8315036 16.6749439,55.9281775 15.6080116,56.2140611 C14.5410794,56.4999447 13.4444055,55.8667797 13.1585219,54.7998475 C12.8726383,53.7329153 13.5058033,52.6362414 14.5727355,52.3503578 C15.6396677,52.0644742 16.7363416,52.6976391 17.0222252,53.7645714 Z M16,46 C16,47.1045695 15.1045695,48 14,48 C12.8954305,48 12,47.1045695 12,46 C12,44.8954305 12.8954305,44 14,44 C15.1045695,44 16,44.8954305 16,46 Z M17.0222252,38.2354286 C16.7363416,39.3023609 15.6396677,39.9355258 14.5727355,39.6496422 C13.5058033,39.3637586 12.8726383,38.2670847 13.1585219,37.2001525 C13.4444055,36.1332203 14.5410794,35.5000553 15.6080116,35.7859389 C16.6749439,36.0718225 17.3081088,37.1684964 17.0222252,38.2354286 Z M20.0192379,31 C19.4669531,31.9565852 18.2437723,32.2843356 17.2871871,31.7320508 C16.3306018,31.1797661 16.0028515,29.9565852 16.5551363,29 C17.107421,28.0434148 18.3306018,27.7156644 19.2871871,28.2679492 C20.2437723,28.8202339 20.5715226,30.0434148 20.0192379,31 Z M24.7867966,24.7867966 C24.005748,25.5678451 22.739418,25.5678451 21.9583694,24.7867966 C21.1773209,24.005748 21.1773209,22.739418 21.9583694,21.9583694 C22.739418,21.1773209 24.005748,21.1773209 24.7867966,21.9583694 C25.5678451,22.739418 25.5678451,24.005748 24.7867966,24.7867966 Z M31,20.0192379 C30.0434148,20.5715226 28.8202339,20.2437723 28.2679492,19.2871871 C27.7156644,18.3306018 28.0434148,17.107421 29,16.5551363 C29.9565852,16.0028515 31.1797661,16.3306018 31.7320508,17.2871871 C32.2843356,18.2437723 31.9565852,19.4669531 31,20.0192379 Z M38.2354286,17.0222252 C37.1684964,17.3081088 36.0718225,16.6749439 35.7859389,15.6080116 C35.5000553,14.5410794 36.1332203,13.4444055 37.2001525,13.1585219 C38.2670847,12.8726383 39.3637586,13.5058033 39.6496422,14.5727355 C39.9355258,15.6396677 39.3023609,16.7363416 38.2354286,17.0222252 Z"
                                  id="Oval-4" fill="#FFFFFF"></path>
                                <path
                                  d="M46,6 C44.3431458,6 43,4.65685425 43,3 C43,1.34314575 44.3431458,0 46,0 C47.6568542,0 49,1.34314575 49,3 C49,4.65685425 47.6568542,6 46,6 Z M56.3527618,7.36296695 C54.7523635,6.93414151 53.802616,5.28913064 54.2314415,3.68873233 C54.6602669,2.08833402 56.3052778,1.13858656 57.9056761,1.56741199 C59.5060744,1.99623743 60.4558219,3.64124829 60.0269964,5.2416466 C59.598171,6.84204491 57.9531601,7.79179238 56.3527618,7.36296695 Z M66,11.3589838 C64.5651221,10.5305567 64.0734967,8.69578551 64.9019238,7.26090764 C65.7303509,5.82602977 67.5651221,5.3344043 69,6.16283143 C70.4348779,6.99125855 70.9265033,8.82602977 70.0980762,10.2609076 C69.2696491,11.6957855 67.4348779,12.187411 66,11.3589838 Z M74.2842712,17.7157288 C73.1126984,16.5441559 73.1126984,14.6446609 74.2842712,13.4730881 C75.4558441,12.3015152 77.3553391,12.3015152 78.5269119,13.4730881 C79.6984848,14.6446609 79.6984848,16.5441559 78.5269119,17.7157288 C77.3553391,18.8873016 75.4558441,18.8873016 74.2842712,17.7157288 Z M80.6410162,26 C79.812589,24.5651221 80.3042145,22.7303509 81.7390924,21.9019238 C83.1739702,21.0734967 85.0087414,21.5651221 85.8371686,23 C86.6655957,24.4348779 86.1739702,26.2696491 84.7390924,27.0980762 C83.3042145,27.9265033 81.4694433,27.4348779 80.6410162,26 Z M84.6370331,35.6472382 C84.2082076,34.0468399 85.1579551,32.401829 86.7583534,31.9730036 C88.3587517,31.5441781 90.0037626,32.4939256 90.432588,34.0943239 C90.8614134,35.6947222 89.911666,37.3397331 88.3112677,37.7685585 C86.7108694,38.197384 85.0658585,37.2476365 84.6370331,35.6472382 Z M86,46 C86,44.3431458 87.3431458,43 89,43 C90.6568542,43 92,44.3431458 92,46 C92,47.6568542 90.6568542,49 89,49 C87.3431458,49 86,47.6568542 86,46 Z M84.6370331,56.3527618 C85.0658585,54.7523635 86.7108694,53.802616 88.3112677,54.2314415 C89.911666,54.6602669 90.8614134,56.3052778 90.432588,57.9056761 C90.0037626,59.5060744 88.3587517,60.4558219 86.7583534,60.0269964 C85.1579551,59.598171 84.2082076,57.9531601 84.6370331,56.3527618 Z M80.6410162,66 C81.4694433,64.5651221 83.3042145,64.0734967 84.7390924,64.9019238 C86.1739702,65.7303509 86.6655957,67.5651221 85.8371686,69 C85.0087414,70.4348779 83.1739702,70.9265033 81.7390924,70.0980762 C80.3042145,69.2696491 79.812589,67.4348779 80.6410162,66 Z M74.2842712,74.2842712 C75.4558441,73.1126984 77.3553391,73.1126984 78.5269119,74.2842712 C79.6984848,75.4558441 79.6984848,77.3553391 78.5269119,78.5269119 C77.3553391,79.6984848 75.4558441,79.6984848 74.2842712,78.5269119 C73.1126984,77.3553391 73.1126984,75.4558441 74.2842712,74.2842712 Z M66,80.6410162 C67.4348779,79.812589 69.2696491,80.3042145 70.0980762,81.7390924 C70.9265033,83.1739702 70.4348779,85.0087414 69,85.8371686 C67.5651221,86.6655957 65.7303509,86.1739702 64.9019238,84.7390924 C64.0734967,83.3042145 64.5651221,81.4694433 66,80.6410162 Z M56.3527618,84.6370331 C57.9531601,84.2082076 59.598171,85.1579551 60.0269964,86.7583534 C60.4558219,88.3587517 59.5060744,90.0037626 57.9056761,90.432588 C56.3052778,90.8614134 54.6602669,89.911666 54.2314415,88.3112677 C53.802616,86.7108694 54.7523635,85.0658585 56.3527618,84.6370331 Z M46,86 C47.6568542,86 49,87.3431458 49,89 C49,90.6568542 47.6568542,92 46,92 C44.3431458,92 43,90.6568542 43,89 C43,87.3431458 44.3431458,86 46,86 Z M35.6472382,84.6370331 C37.2476365,85.0658585 38.197384,86.7108694 37.7685585,88.3112677 C37.3397331,89.911666 35.6947222,90.8614134 34.0943239,90.432588 C32.4939256,90.0037626 31.5441781,88.3587517 31.9730036,86.7583534 C32.401829,85.1579551 34.0468399,84.2082076 35.6472382,84.6370331 Z M26,80.6410162 C27.4348779,81.4694433 27.9265033,83.3042145 27.0980762,84.7390924 C26.2696491,86.1739702 24.4348779,86.6655957 23,85.8371686 C21.5651221,85.0087414 21.0734967,83.1739702 21.9019238,81.7390924 C22.7303509,80.3042145 24.5651221,79.812589 26,80.6410162 Z M17.7157288,74.2842712 C18.8873016,75.4558441 18.8873016,77.3553391 17.7157288,78.5269119 C16.5441559,79.6984848 14.6446609,79.6984848 13.4730881,78.5269119 C12.3015152,77.3553391 12.3015152,75.4558441 13.4730881,74.2842712 C14.6446609,73.1126984 16.5441559,73.1126984 17.7157288,74.2842712 Z M11.3589838,66 C12.187411,67.4348779 11.6957855,69.2696491 10.2609076,70.0980762 C8.82602977,70.9265033 6.99125855,70.4348779 6.16283143,69 C5.3344043,67.5651221 5.82602977,65.7303509 7.26090764,64.9019238 C8.69578551,64.0734967 10.5305567,64.5651221 11.3589838,66 Z M7.36296695,56.3527618 C7.79179238,57.9531601 6.84204491,59.598171 5.2416466,60.0269964 C3.64124829,60.4558219 1.99623743,59.5060744 1.56741199,57.9056761 C1.13858656,56.3052778 2.08833402,54.6602669 3.68873233,54.2314415 C5.28913064,53.802616 6.93414151,54.7523635 7.36296695,56.3527618 Z M6,46 C6,47.6568542 4.65685425,49 3,49 C1.34314575,49 1.97357338e-16,47.6568542 -1.0700185e-16,46 C-4.11361038e-16,44.3431458 1.34314575,43 3,43 C4.65685425,43 6,44.3431458 6,46 Z M7.36296695,35.6472382 C6.93414151,37.2476365 5.28913064,38.197384 3.68873233,37.7685585 C2.08833402,37.3397331 1.13858656,35.6947222 1.56741199,34.0943239 C1.99623743,32.4939256 3.64124829,31.5441781 5.2416466,31.9730036 C6.84204491,32.401829 7.79179238,34.0468399 7.36296695,35.6472382 Z M11.3589838,26 C10.5305567,27.4348779 8.69578551,27.9265033 7.26090764,27.0980762 C5.82602977,26.2696491 5.3344043,24.4348779 6.16283143,23 C6.99125855,21.5651221 8.82602977,21.0734967 10.2609076,21.9019238 C11.6957855,22.7303509 12.187411,24.5651221 11.3589838,26 Z M17.7157288,17.7157288 C16.5441559,18.8873016 14.6446609,18.8873016 13.4730881,17.7157288 C12.3015152,16.5441559 12.3015152,14.6446609 13.4730881,13.4730881 C14.6446609,12.3015152 16.5441559,12.3015152 17.7157288,13.4730881 C18.8873016,14.6446609 18.8873016,16.5441559 17.7157288,17.7157288 Z M26,11.3589838 C24.5651221,12.187411 22.7303509,11.6957855 21.9019238,10.2609076 C21.0734967,8.82602977 21.5651221,6.99125855 23,6.16283143 C24.4348779,5.3344043 26.2696491,5.82602977 27.0980762,7.26090764 C27.9265033,8.69578551 27.4348779,10.5305567 26,11.3589838 Z M35.6472382,7.36296695 C34.0468399,7.79179238 32.401829,6.84204491 31.9730036,5.2416466 C31.5441781,3.64124829 32.4939256,1.99623743 34.0943239,1.56741199 C35.6947222,1.13858656 37.3397331,2.08833402 37.7685585,3.68873233 C38.197384,5.28913064 37.2476365,6.93414151 35.6472382,7.36296695 Z"
                                  id="Oval-5" fill="#FFFFFF"></path>
                              </g>
                              <g id="graph" transform="translate(0.000000, 230.000000)">
                                <path
                                  d="M-1.26217745e-29,91 C1.33333333,91 2.33333333,91 3,91 C4,91 6,85 7,85 C8,85 10,93 10.5,93 C11,93 13,89 14,89 C15,89 16,94 17,94 C18,94 19,91 20,91 C21,91 27,91 28,91 C29,91 29,89 30,89 C31,89 34.5,98 36,98 C37.5,98 49,98 50,98 C51,98 54,103 56,103 C58,103 65,82 67,82 C69,82 73.5,82 75,82 C76.5,82 83,100 88,100 C89.9747213,100 91,96 92,96 C93,96 97,96 98,96 C99,96 105.0347,84 108,84 C110.9653,84 111,84 112,84 C113,84 114,72 116,72 C118,72 119,72 123,72 C127,72 131.998214,54 133,54 C135.030897,54 138,59 140,59 C142,59 145,59 146,59 C147,59 148,52 149,52 C153,52 155,37 156,37 C157,37 161,47 163,47 C165,47 170,46 172,46 C174,46 174,49 175,49 C176,49 180,32 182,32 C184,32 185,39 186,39 C187,39 190,39 192,39 C194,39 195,49 196,49 C197,49 199,44 201,44 C203,44 206,49 209,49 C212,49 213,48 215,48 C217,48 218,53 219,53 C220,53 227.5,43 229,43 C230.5,43 231,48 233,48 C235,48 238,48 239,48 C240,48 243,45 245,45 C247,45 249,63 250,63 C251,63 255,56 256,56 C257,56 262,56 263,56 C264,56 265,69 266,69 C267,69 274,43 279,43 L304,43"
                                  id="Path-2" stroke="#FFFFFF" stroke-width="1.5" opacity="0.5"></path>
                                <circle id="Oval-2" fill="#FFFFFF" cx="304" cy="43" r="6"></circle>
                                <text id="10.450,87-kr" font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="16" font-weight="normal" letter-spacing="0.200000003" fill="#0CC47D">
                                  <tspan x="262.220312" y="15">10.450,87 k</tspan>
                                  <tspan x="340.475" y="15">r</tspan>
                                </text>
                                <text id="Feb" font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="16" font-weight="normal" letter-spacing="0.200000003" fill-opacity="0.35"
                                      fill="#FFFFFF">
                                  <tspan x="96" y="152">Fe</tspan>
                                  <tspan x="112.767188" y="152">b</tspan>
                                </text>
                                <text id="Mar" font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="16" font-weight="normal" letter-spacing="0.200000003" fill-opacity="0.35"
                                      fill="#FFFFFF">
                                  <tspan x="184" y="152">Ma</tspan>
                                  <tspan x="204.876563" y="152">r</tspan>
                                </text>
                                <text id="Apr" font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="16" font-weight="normal" letter-spacing="0.200000003" fill-opacity="0.35"
                                      fill="#FFFFFF">
                                  <tspan x="272" y="152">Ap</tspan>
                                  <tspan x="290.642188" y="152">r</tspan>
                                </text>
                                <path d="M87.5,146 L87.5,100" id="Path-3" stroke-opacity="0.35" stroke="#FFFFFF"></path>
                                <path d="M176,146.5 L176,48" id="Path-3" stroke-opacity="0.35" stroke="#FFFFFF"></path>
                                <path d="M263.5,146 L263.5,57.5" id="Path-3" stroke-opacity="0.35" stroke="#FFFFFF"></path>
                              </g>
                              <g id="amount" transform="translate(31.000000, 432.000000)">
                                <text id="+-409-kr." font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="28" font-weight="normal" letter-spacing="0.5" fill="#0CC47D">
                                  <tspan x="180.697266" y="57">+ 409 kr.</tspan>
                                </text>
                                <text id="10.451-kr." font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="28" font-weight="normal" letter-spacing="0.5" fill="#FFFFFF">
                                  <tspan x="0.90625" y="57">10.451 kr.</tspan>
                                </text>
                                <text id="AFKAST-I-KR." font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="16" font-weight="normal" letter-spacing="0.5" fill-opacity="0.5"
                                      fill="#FFFFFF">
                                  <tspan x="187.28125" y="16">AFKAST I KR.</tspan>
                                </text>
                                <text id="DU-HAR-NU" font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="16" font-weight="normal" letter-spacing="0.5" fill-opacity="0.5"
                                      fill="#FFFFFF">
                                  <tspan x="16.7226562" y="16">DU HAR NU</tspan>
                                </text>
                                <path d="M145,64 L145,0" id="Path-3" stroke-opacity="0.35" stroke="#FFFFFF"></path>
                              </g>
                              <g id="date" transform="translate(37.000000, 143.000000)">
                                <rect id="Rectangle-2" fill="#0CC47D" x="74" y="23" width="38" height="2"></rect>
                                <text id="1-md." font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="16" font-weight="normal" letter-spacing="0.200000003" fill="#FFFFFF">
                                  <tspan x="0.838281244" y="15">1 md</tspan>
                                  <tspan x="33.5054688" y="15">.</tspan>
                                </text>
                                <text id="3-md." font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="16" font-weight="normal" letter-spacing="0.200000003" fill="#FFFFFF">
                                  <tspan x="74.5726562" y="15">3 md</tspan>
                                  <tspan x="109.771094" y="15">.</tspan>
                                </text>
                                <text id="2017" font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="16" font-weight="normal" letter-spacing="0.200000003" fill="#FFFFFF">
                                  <tspan x="149.66875" y="15">201</tspan>
                                  <tspan x="173.964063" y="15">7</tspan>
                                </text>
                                <text id="Fra-start" font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                      font-size="16" font-weight="normal" letter-spacing="0.200000003" fill="#FFFFFF">
                                  <tspan x="220.039844" y="15">Fra star</tspan>
                                  <tspan x="272.710156" y="15">t</tspan>
                                </text>
                              </g>
                              <g id="dots" transform="translate(154.000000, 82.000000)">
                                <circle id="Oval" stroke="#FFFFFF" stroke-width="1.5" cx="6" cy="6" r="5.25"></circle>
                                <circle id="Oval" fill="#FFFFFF" cx="38" cy="6" r="6"></circle>
                              </g>
                              <g id="logo" transform="translate(129.000000, 40.000000)">
                                <polygon id="e" fill="#FFFFFF"
                                         points="93.5 0 93.5661376 2.96296296 85.5 2.96296296 85.5 13.037037 93.5 13.037037 93.5 16 81.5 16 81.5 0"></polygon>
                                <rect id="Rectangle" fill="#DE4461" x="78" y="6" width="14" height="1"></rect>
                                <rect id="Rectangle" fill="#DE4461" x="78" y="9" width="14" height="1"></rect>
                                <polygon id="n" fill="#FFFFFF"
                                         points="53.5 0 57 0 64 10 64 0 67.5 0 67.5 16 64 16 57 6 57 16 53.5 16"></polygon>
                                <path
                                  d="M24.5,0 L24.5,9.5 C24.5,12 25,16.5 31.5,16.5 C38,16.5 38.5,12 38.5,9.5 L38.5,0 L35,0 L35,9.5 C35,12 33.8571429,13.5 31.4285714,13.5 C29,13.5 28,12 28,9.5 L28,0 L24.5,0 Z"
                                  id="u" fill="#FFFFFF"></path>
                                <path
                                  d="M2.5,11.5 L0,13.5 C1.33333333,15.1666667 3,16 5,16 C8,16 11,14.5222478 11,10.2857143 L11,0 L7.5,0 L7.5,10.5 C7.5,12 6,12.5 5,12.5 C4.33333333,12.5 3.5,12.1666667 2.5,11.5 Z"
                                  id="j" fill="#FFFFFF"></path>
                              </g>
                            </g>
                            <g id="screen" stroke-linejoin="round">
                              <use fill-opacity="0.6" fill="url(#linearGradient-14)" fill-rule="evenodd"
                                   xlinkHref="#path-16"></use>
                              <rect stroke="url(#linearGradient-15)" stroke-width="4" x="30" y="102" width="356"
                                    height="644"></rect>
                            </g>
                            <g id="speaker">
                              <use fill="#2A2C2E" fill-rule="evenodd" xlinkHref="#path-18"></use>
                              <rect stroke="url(#linearGradient-17)" stroke-width="3" x="170.5" y="52.5" width="75"
                                    height="8" rx="3"></rect>
                            </g>
                            <g id="camera">
                              <use fill="url(#radialGradient-19)" fill-rule="evenodd" xlinkHref="#path-21"></use>
                              <circle stroke="url(#linearGradient-20)" stroke-width="3" cx="144" cy="56" r="7.5"></circle>
                            </g>
                            <g id="sensor">
                              <use fill="#2A2C2E" fill-rule="evenodd" xlinkHref="#path-23"></use>
                              <circle stroke="url(#linearGradient-22)" stroke-width="2" cx="208" cy="32" r="5"></circle>
                            </g>
                          </g>
                        </g>
                      </svg>
                    </div>
                  )}

                  {device === "macbook" && (
                    <div className="macbook-padding-hack">
                      <svg className="case__svg" viewBox="0 0 732 744" version="1.1" xmlns="http://www.w3.org/2000/svg"
                           xmlnsXlink="http://www.w3.org/1999/xlink">
                        <title>macbook</title>
                        <defs>
                          <linearGradient x1="50.2325058%" y1="0%" x2="50.2417326%" y2="100%" id="linearGradient-1a">
                            <stop stop-color="#0A0C0E" offset="0%"></stop>
                            <stop stop-color="#0A0C0E" offset="10%"></stop>
                            <stop stop-color="#0A0C0E" offset="95.2816819%"></stop>
                            <stop stop-color="#1A1C1E" offset="95.3025596%"></stop>
                            <stop stop-color="#1A1C1E" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="0%" y1="0%" x2="100%" y2="100%" id="linearGradient-2b">
                            <stop stop-color="#FFFFFF" stop-opacity="0.5" offset="0%"></stop>
                            <stop stop-color="#FFFFFF" stop-opacity="0" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient-3b">
                            <stop stop-color="#EAECEE" offset="0%"></stop>
                            <stop stop-color="#9A9C9E" offset="100%"></stop>
                          </linearGradient>
                          <path
                            d="M34.9268326,0.699441341 L606.926833,0.699441341 L606.926833,720.699441 L16.9268326,720.699441 L16.9268326,720.699441 C8.09027664,720.699441 0.926832642,713.535997 0.926832642,704.699441 L0.926832642,34.6994413 L0.926832642,34.6994413 C0.926832642,15.9217598 16.1491511,0.699441341 34.9268326,0.699441341 Z"
                            id="path--4"></path>
                          <rect id="path--5" x="8.52651283e-13" y="0" width="568" height="616"></rect>
                          <path
                            d="M0,0 L38,0 L38,38 L0,38 L0,0 Z M19,33.4032258 C26.954682,33.4032258 33.4032258,26.954682 33.4032258,19 C33.4032258,11.045318 26.954682,4.59677419 19,4.59677419 C11.045318,4.59677419 4.59677419,11.045318 4.59677419,19 C4.59677419,26.954682 11.045318,33.4032258 19,33.4032258 Z"
                            id="path--7"></path>
                          <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient-9b">
                            <stop stop-color="#D4215F" stop-opacity="0" offset="0%"></stop>
                            <stop stop-color="#D4215F" stop-opacity="0.9" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="0%" y1="0%" x2="100%" y2="100%" id="linearGradient-10b">
                            <stop stop-color="#FFFFFF" stop-opacity="0.7" offset="0%"></stop>
                            <stop stop-color="#000000" stop-opacity="0.1" offset="100%"></stop>
                          </linearGradient>
                          <radialGradient cx="50%" cy="59.2669947%" fx="50%" fy="59.2669947%" r="53.8948642%"
                                          gradientTransform="translate(0.500000,0.592670),scale(1.000000,0.999570),rotate(64.321539),translate(-0.500000,-0.592670)"
                                          id="radialGradient-11">
                            <stop stop-color="#0000FF" offset="0%"></stop>
                            <stop stop-color="#000000" offset="100%"></stop>
                          </radialGradient>
                          <ellipse id="path--12" cx="526.425759" cy="26.1994413" rx="2.49892612" ry="2.5"></ellipse>
                          <linearGradient x1="49.9970824%" y1="100%" x2="49.9970824%" y2="0%" id="linearGradient-13b">
                            <stop stop-color="#9A9C9E" offset="0%"></stop>
                            <stop stop-color="#C0C2C4" offset="72.8401059%"></stop>
                            <stop stop-color="#D0D2D4" offset="100%"></stop>
                          </linearGradient>
                          <radialGradient cx="50%" cy="25.1953125%" fx="50%" fy="25.1953125%" r="74.8046875%"
                                          gradientTransform="translate(0.500000,0.251953),scale(0.250107,1.000000),rotate(90.000000),scale(1.000000,2.675107),translate(-0.500000,-0.251953)"
                                          id="radialGradient-14">
                            <stop stop-color="#000000" stop-opacity="0.5" offset="0%"></stop>
                            <stop stop-color="#000000" stop-opacity="0.25" offset="47.5195822%"></stop>
                            <stop stop-color="#000000" stop-opacity="0" offset="100%"></stop>
                          </radialGradient>
                          <linearGradient x1="0%" y1="0%" x2="100%" y2="0%" id="linearGradient-15b">
                            <stop stop-color="#D2D4D6" offset="0%"></stop>
                            <stop stop-color="#E2E4E6" offset="1.35%"></stop>
                            <stop stop-color="#EAECEE" offset="5.45%"></stop>
                            <stop stop-color="#E0E2E4" offset="100%"></stop>
                          </linearGradient>
                          <polygon id="path--16" points="0 3.69218782e-14 736 3.63875596e-14 736 7 0 7"></polygon>
                          <filter x="-0.3%" y="-28.6%" width="100.5%" height="157.1%" filterUnits="objectBoundingBox"
                                  id="filter--17">
                            <feGaussianBlur stdDeviation="1.5" in="SourceAlpha" result="shadowBlurInner1"></feGaussianBlur>
                            <feOffset dx="0" dy="-1" in="shadowBlurInner1" result="shadowOffsetInner1"></feOffset>
                            <feComposite in="shadowOffsetInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1"
                                         result="shadowInnerInner1"></feComposite>
                            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 0.5 0" type="matrix"
                                           in="shadowInnerInner1"></feColorMatrix>
                          </filter>
                          <linearGradient x1="0.0397819281%" y1="47.0776585%" x2="100.040845%" y2="47.0776585%"
                                          id="linearGradient-18b">
                            <stop stop-color="#9A9C9E" offset="0%"></stop>
                            <stop stop-color="#EAECEE" offset="10.21%"></stop>
                            <stop stop-color="#EAECEE" offset="100%"></stop>
                          </linearGradient>
                          <path
                            d="M533,-2.49245069e-14 L533,7 C533,7 539,10 554,10 L736,10 L736,-2.49245069e-14 L533,-2.49245069e-14 Z"
                            id="path--19"></path>
                          <filter x="-1.0%" y="-20.0%" width="102.0%" height="140.0%" filterUnits="objectBoundingBox"
                                  id="filter--20">
                            <feGaussianBlur stdDeviation="1.5" in="SourceAlpha" result="shadowBlurInner1"></feGaussianBlur>
                            <feOffset dx="0" dy="-1" in="shadowBlurInner1" result="shadowOffsetInner1"></feOffset>
                            <feComposite in="shadowOffsetInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1"
                                         result="shadowInnerInner1"></feComposite>
                            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 0.5 0" type="matrix"
                                           in="shadowInnerInner1"></feColorMatrix>
                          </filter>
                        </defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g id="macbook">
                            <g id="lid" transform="translate(129.000000, 0.000000)">
                              <g>
                                <use fill="url(#linearGradient-1a)" fill-rule="evenodd" xlinkHref="#path--4"></use>
                                <use fill="url(#linearGradient-2b)" fill-rule="evenodd" xlinkHref="#path--4"></use>
                                <path stroke="url(#linearGradient-3b)" stroke-width="6"
                                      d="M34.9268326,-2.30055866 L609.926833,-2.30055866 L609.926833,723.699441 L16.9268326,723.699441 C6.43342239,723.699441 -2.07316736,715.192852 -2.07316736,704.699441 L-2.07316736,34.6994413 C-2.07316736,14.2649056 14.4922969,-2.30055866 34.9268326,-2.30055866 Z"></path>
                                <use stroke="#121416" stroke-width="3" xlinkHref="#path--4"></use>
                              </g>
                              <path
                                d="M504.586804,698.975905 L504.532313,698.975905 C504.248424,699.9511 502.990096,703.54936 500.757327,709.770686 L499.535326,709.770686 C497.462666,703.562147 496.308945,699.963551 496.074165,698.974896 L495.46266,709.84539 L493.926833,709.84539 L494.796664,697.364714 L496.85116,697.364714 C498.899602,703.327603 500.028768,706.740784 500.238658,707.604259 L500.294157,707.604259 C500.503374,706.765013 501.681313,703.351831 503.827972,697.364714 L505.883478,697.364714 L506.660473,709.84539 L505.08731,709.84539 L504.586804,698.975905 Z M514.026792,708.715739 L513.972301,708.715739 C513.471795,709.419373 512.509129,710.049313 511.232636,710.049313 C509.418302,710.049313 508.492971,708.772272 508.492971,707.47605 C508.492971,705.309624 510.418305,704.124449 513.879466,704.142621 L513.879466,703.956869 C513.879466,703.216892 513.675631,701.883318 511.843133,701.883318 C511.010638,701.883318 510.139798,702.141755 509.510129,702.5496 L509.140804,701.475473 C509.880463,700.994942 510.95312,700.678963 512.083295,700.678963 C514.821951,700.678963 515.488956,702.55061 515.488956,704.345534 L515.488956,707.696125 C515.488956,708.474464 515.525283,709.233622 515.636282,709.84438 L514.155955,709.84438 L514.026792,708.715739 Z M513.915793,705.272272 C512.138794,705.235929 510.121634,705.549889 510.121634,707.290299 C510.121634,708.347265 510.824966,708.845967 511.65847,708.845967 C512.823963,708.845967 513.563622,708.106999 513.822957,707.347841 C513.879466,707.181271 513.915793,706.99451 513.915793,706.827939 L513.915793,705.272272 Z M524.504119,709.512249 C524.078285,709.735352 523.134791,710.032151 521.930953,710.032151 C519.228624,710.032151 517.470798,708.197856 517.470798,705.458023 C517.470798,702.699009 519.357787,700.700163 522.283124,700.700163 C523.244781,700.700163 524.096449,700.940428 524.540446,701.163532 L524.171121,702.422401 C523.781614,702.199298 523.171118,701.996384 522.283124,701.996384 C520.228627,701.996384 519.117625,703.5147 519.117625,705.383319 C519.117625,707.457879 520.449617,708.73391 522.226615,708.73391 C523.152955,708.73391 523.763451,708.493645 524.226621,708.289722 L524.504119,709.512249 Z M526.464771,697.533304 C527.169112,697.384904 528.279105,697.274867 529.408271,697.274867 C531.01877,697.274867 532.054091,697.550465 532.832096,698.180405 C533.478919,698.662955 533.868426,699.401922 533.868426,700.385193 C533.868426,701.587529 533.072258,702.642476 531.758429,703.125026 C532.943095,703.458167 534.330586,704.439419 534.330586,706.290876 C534.330586,707.365003 533.904753,708.180694 533.276093,708.790443 C532.406262,709.586953 530.999598,709.957447 528.964274,709.957447 C527.853271,709.957447 527.002613,709.883752 526.464771,709.810057 L526.464771,697.533304 Z M529.537434,702.642476 C531.240769,702.642476 532.239763,701.7541 532.239763,700.551764 C532.239763,699.088971 531.128761,698.513546 529.500098,698.513546 C528.760439,698.513546 528.334605,698.569069 528.07527,698.625602 L528.07527,702.642476 L529.537434,702.642476 Z M528.07527,708.622863 C528.390104,708.680406 528.853274,708.697568 529.426435,708.697568 C531.092433,708.697568 532.62927,708.087818 532.62927,706.272704 C532.62927,704.568637 531.166097,703.865003 529.408271,703.865003 L528.07527,703.865003 L528.07527,708.622863 Z M540.161078,710.050322 C537.737257,710.050322 535.867423,708.27256 535.867423,705.440861 C535.867423,702.440573 537.829083,700.681991 540.309414,700.681991 C542.88258,700.680982 544.621233,702.55061 544.621233,705.291452 C544.621233,708.605701 542.326574,710.050322 540.161078,710.050322 Z M540.234741,708.826786 C541.789741,708.826786 542.956243,707.365003 542.956243,705.346976 C542.956243,703.827651 542.197411,701.903509 540.272078,701.903509 C538.346744,701.903509 537.514249,703.681271 537.514249,705.383319 C537.514249,707.345822 538.643415,708.826786 540.234741,708.826786 Z M550.323571,710.050322 C547.899749,710.050322 546.029915,708.27256 546.029915,705.440861 C546.029915,702.440573 547.991576,700.681991 550.471906,700.681991 C553.044063,700.680982 554.783726,702.55061 554.783726,705.291452 C554.783726,708.605701 552.489067,710.050322 550.323571,710.050322 Z M550.397234,708.826786 C551.952234,708.826786 553.118736,707.365003 553.118736,705.346976 C553.118736,703.827651 552.359904,701.903509 550.43457,701.903509 C548.509236,701.903509 547.676742,703.681271 547.676742,705.383319 C547.676742,707.345822 548.805907,708.826786 550.397234,708.826786 Z M558.485048,704.994654 L561.908873,700.884905 L563.870534,700.884905 L560.409373,704.567628 L564.351867,709.8464 L562.372043,709.8464 L559.280207,705.550899 L558.447712,706.476627 L558.447712,709.8464 L556.837213,709.8464 L556.837213,696.699441 L558.447712,696.699441 L558.447712,704.994654 L558.485048,704.994654 Z"
                                id="macbook" fill="#9A9C9E" fill-rule="nonzero"></path>
                              <g id="sustainia" transform="translate(35.000000, 47.000000)">
                                <mask id="mask-6" fill="white">
                                  <use xlinkHref="#path--5"></use>
                                </mask>
                                <use id="background" fill="#FFFFFF" xlinkHref="#path--5"></use>
                                <text id="body-text" mask="url(#mask-6)" font-family="Georgia" font-size="13"
                                      font-weight="normal" line-spacing="20" letter-spacing="0.200000003" fill="#666666">
                                  <tspan x="264.32" y="534.753333">A team of partners, including Parley for the Oceans and
                                  </tspan>
                                  <tspan x="264.32" y="554.753333">adidas, has created an innovative shoe, the upper part
                                    of
                                  </tspan>
                                  <tspan x="264.32" y="574.753333">which is made entirely of yarns and laments reclaimed
                                  </tspan>
                                  <tspan x="264.32" y="594.753333">and recycled from ocean waste and deep-sea fishing
                                    nets.
                                  </tspan>
                                  <tspan x="264.32" y="614.753333">Removing 72 km of illegally abandoned deepsea netting,
                                  </tspan>
                                  <tspan x="601.253106" y="614.753333"></tspan>
                                </text>
                                <text id="abstract" mask="url(#mask-6)"
                                      font-family="NeueHaasUnicaPro-Heavy, Neue Haas Unica Pro" font-size="18"
                                      font-weight="600" line-spacing="27" letter-spacing="0.200000003" fill="#0A0A0A">
                                  <tspan x="264.32" y="443.456667">Adidas and Parley for the Oceans are re</tspan>
                                  <tspan x="264.32" y="470.456667">design, material use, and 3D-printing in</tspan>
                                  <tspan x="264.32" y="497.456667">running shoe made from ocean plastic</tspan>
                                  <tspan x="602.144" y="497.456667">.</tspan>
                                </text>
                                <g id="publisher" mask="url(#mask-6)">
                                  <g transform="translate(37.760000, 427.093333)">
                                    <text id="company" fill="none" opacity="0.65"
                                          font-family="SanFranciscoDisplay-Medium, San Francisco Display" font-size="11"
                                          font-weight="400" line-spacing="12" letter-spacing="0.5">
                                      <tspan x="47.2" y="55.5999997" fill="#0A0C0E">Sustaini</tspan>
                                      <tspan x="90.4197266" y="55.5999997" fill="#0A0C0E">a</tspan>
                                    </text>
                                    <g id="Logo-opacity-248" fill="none" transform="translate(0.000000, 33.466667)"
                                       fill-rule="evenodd">
                                      <g id="colors" stroke-width="1">
                                        <mask id="mask-8" fill="white">
                                          <use xlinkHref="#path--7"></use>
                                        </mask>
                                        <g id="mask"></g>
                                        <polygon id="purple" fill="#744B8A" mask="url(#mask-8)"
                                                 points="32.8669355 17.4677419 36.4677419 23.9798387 33.0967742 31.4879032 27.8870968 30.4919355 28.0403226 33.4798387 26.5846774 31.8709677 22.4475806 34.5524194 22.6008065 35.3185484 22.4475806 37.7701613 21.6814516 35.0887097 19.5362903 36.5443548 16.625 32.7903226"></polygon>
                                        <polygon id="orange" fill="#DE5646" mask="url(#mask-8)"
                                                 points="28.3467742 27.8870968 24.6693548 37.8467742 22.4475806 32.1008065 32.9435484 17.0080645 34.858871 19.5362903 33.25 26.0483871"></polygon>
                                        <polygon id="purple" fill="#744B8A" mask="url(#mask-8)"
                                                 points="30.1854839 10.9556452 34.2459677 9.80645161 33.5564516 11.9516129 36.4677419 11.1854839 35.1653226 14.4798387 32.8669355 16.0120968"></polygon>
                                        <polygon id="blue" fill="#45B7B5" mask="url(#mask-8)"
                                                 points="28.5 9.80645161 36.0080645 14.0967742 33.1733871 18.6935484 38 19.1532258 36.0080645 19.9959677 32.5604839 20.1491935 36.6975806 20.9919355 37.9233871 22.7540323 31.7943548 23.2137097 35.0887097 28.7298387 31.4112903 29.4959677 29.4193548 28.3467742"></polygon>
                                        <polygon id="orange" fill="#DE5646" mask="url(#mask-8)"
                                                 points="14.0201613 1.60887097 22.0645161 0.995967742 28.1169355 8.65725806 12.8709677 6.74193548"></polygon>
                                        <polygon id="green" fill="#64B14D" mask="url(#mask-8)"
                                                 points="18.6935484 6.43548387 25.5120968 4.13709677 33.8629032 6.20564516 31.5645161 12.4112903 9.19354839 9.04032258 9.19354839 3.98387097 14.0967742 1.22580645"></polygon>
                                        <polygon id="blue" fill="#45B7B5" mask="url(#mask-8)"
                                                 points="20.608871 5.05645161 24.3629032 1.30241935 28.3467742 4.36693548 29.4959677 9.65322581 12.1048387 7.2016129 10.4959677 2.52822581 12.7943548 3.29435484 14.4032258 5.82258065"></polygon>
                                        <polygon id="pink" fill="#D32260" mask="url(#mask-8)"
                                                 points="3.67741935 8.65725806 5.43951613 8.12096774 6.51209677 8.81048387 5.05645161 5.74596774 12.0282258 4.90322581 16.8548387 0 22.141129 7.50806452 24.8991935 2.91129032 27.4274194 2.2983871 25.8951613 7.125 6.66532258 11.7983871"></polygon>
                                        <polygon id="pink" fill="#D32260" mask="url(#mask-8)"
                                                 points="2.98790323 28.9596774 1.22580645 22.5241935 3.06451613 20.1491935 0 19.766129 0.995967742 17.6975806 4.8266129 17.9274194 8.65725806 28.7298387"></polygon>
                                        <polygon id="yellow" fill="#F9D73A" mask="url(#mask-8)"
                                                 points="17.6209677 33.25 16.7782258 34.6290323 14.25 34.9354839 13.3306452 37.233871 12.5645161 36.8508065 13.1008065 35.0887097 11.4153226 35.2419355 10.6491935 33.0967742 5.97580645 31.8709677 6.66532258 26.0483871"></polygon>
                                        <polygon id="yellow" fill="#F9D73A" mask="url(#mask-8)"
                                                 points="6.43548387 25.1290323 2.22177419 22.141129 0 16.4717742 3.52419355 16.1653226 2.4516129 9.95967742 8.04435484 10.4193548"></polygon>
                                      </g>
                                      <path
                                        d="M7.82217742,20.5782258 C8.49790323,20.5782258 8.97137097,20.2196774 8.97137097,19.6175 L8.97137097,19.6083065 C8.97137097,19.0750806 8.61741935,18.8314516 7.93709677,18.6659677 C7.32112903,18.518871 7.17403226,18.4085484 7.17403226,18.1603226 L7.17403226,18.151129 C7.17403226,17.9396774 7.36709677,17.7695968 7.69806452,17.7695968 C7.99225806,17.7695968 8.28185484,17.8845161 8.57145161,18.1005645 L8.87483871,17.6730645 C8.54846774,17.4110484 8.17612903,17.2685484 7.70725806,17.2685484 C7.06830645,17.2685484 6.60862903,17.6500806 6.60862903,18.2016935 L6.60862903,18.2108871 C6.60862903,18.803871 6.99475806,19.006129 7.67967742,19.1716129 C8.27725806,19.3095161 8.40596774,19.433629 8.40596774,19.6634677 L8.40596774,19.6726613 C8.40596774,19.9162903 8.18532258,20.0771774 7.83596774,20.0771774 C7.43604839,20.0771774 7.12806452,19.9254839 6.82008065,19.658871 L6.47991935,20.0633871 C6.86604839,20.4081452 7.33032258,20.5782258 7.82217742,20.5782258 Z M10.9571774,20.5828226 C11.7983871,20.5828226 12.3408065,20.1001613 12.3408065,19.1394355 L12.3408065,17.3145161 L11.7754032,17.3145161 L11.7754032,19.1670161 C11.7754032,19.76 11.4674194,20.0633871 10.966371,20.0633871 C10.4607258,20.0633871 10.1527419,19.7416129 10.1527419,19.1440323 L10.1527419,17.3145161 L9.58733871,17.3145161 L9.58733871,19.1670161 C9.58733871,20.1001613 10.1205645,20.5828226 10.9571774,20.5828226 Z M14.243871,20.5782258 C14.9195968,20.5782258 15.3930645,20.2196774 15.3930645,19.6175 L15.3930645,19.6083065 C15.3930645,19.0750806 15.0391129,18.8314516 14.3587903,18.6659677 C13.7428226,18.518871 13.5957258,18.4085484 13.5957258,18.1603226 L13.5957258,18.151129 C13.5957258,17.9396774 13.7887903,17.7695968 14.1197581,17.7695968 C14.4139516,17.7695968 14.7035484,17.8845161 14.9931452,18.1005645 L15.2965323,17.6730645 C14.9701613,17.4110484 14.5978226,17.2685484 14.1289516,17.2685484 C13.49,17.2685484 13.0303226,17.6500806 13.0303226,18.2016935 L13.0303226,18.2108871 C13.0303226,18.803871 13.4164516,19.006129 14.101371,19.1716129 C14.6989516,19.3095161 14.8276613,19.433629 14.8276613,19.6634677 L14.8276613,19.6726613 C14.8276613,19.9162903 14.6070161,20.0771774 14.2576613,20.0771774 C13.8577419,20.0771774 13.5497581,19.9254839 13.2417742,19.658871 L12.9016129,20.0633871 C13.2877419,20.4081452 13.7520161,20.5782258 14.243871,20.5782258 Z M16.7812903,20.5322581 L16.7812903,17.8385484 L15.7608065,17.8385484 L15.7608065,17.3145161 L18.3717742,17.3145161 L18.3717742,17.8385484 L17.3512903,17.8385484 L17.3512903,20.5322581 L16.7812903,20.5322581 Z M18.2798387,20.5322581 L19.6956452,17.2915323 L20.2196774,17.2915323 L21.6354839,20.5322581 L21.0379032,20.5322581 L20.7115323,19.7554032 L19.19,19.7554032 L18.8590323,20.5322581 L18.2798387,20.5322581 Z M19.3968548,19.2543548 L20.5046774,19.2543548 L19.9484677,17.9672581 L19.3968548,19.2543548 Z M22.2192742,20.5322581 L22.7846774,20.5322581 L22.7846774,17.3145161 L22.2192742,17.3145161 L22.2192742,20.5322581 Z M23.6442742,20.5322581 L24.2004839,20.5322581 L24.2004839,18.2430645 L25.9748387,20.5322581 L26.4483065,20.5322581 L26.4483065,17.3145161 L25.8920968,17.3145161 L25.8920968,19.5393548 L24.1683065,17.3145161 L23.6442742,17.3145161 L23.6442742,20.5322581 Z M27.3079032,20.5322581 L27.8733065,20.5322581 L27.8733065,17.3145161 L27.3079032,17.3145161 L27.3079032,20.5322581 Z M28.4570968,20.5322581 L29.8729032,17.2915323 L30.3969355,17.2915323 L31.8127419,20.5322581 L31.2151613,20.5322581 L30.8887903,19.7554032 L29.3672581,19.7554032 L29.0362903,20.5322581 L28.4570968,20.5322581 Z M29.5741129,19.2543548 L30.6819355,19.2543548 L30.1257258,17.9672581 L29.5741129,19.2543548 Z"
                                        id="dark/100%" fill="#0A0C0E"></path>
                                    </g>
                                    <rect id="hr" fill="#DDDDDD" fill-rule="evenodd" x="0.24" y="21.9066667" width="188"
                                          height="1"></rect>
                                    <text id="date" fill="none"
                                          font-family="SanFranciscoDisplay-Regular, San Francisco Display" font-size="11"
                                          font-weight="normal" line-spacing="15" letter-spacing="0.5">
                                      <tspan x="131" y="10.166667" fill="#BBBBBB">Jun 2. 201</tspan>
                                      <tspan x="182.583496" y="10.166667" fill="#BBBBBB">7</tspan>
                                    </text>
                                    <text id="label" fill="none"
                                          font-family="SanFranciscoDisplay-Bold, San Francisco Display" font-size="11"
                                          font-weight="bold" line-spacing="15" letter-spacing="0.5">
                                      <tspan x="0" y="10.9066663" fill="#BBBBBB">PUBLISHE</tspan>
                                      <tspan x="53.4086914" y="10.9066663" fill="#BBBBBB">R</tspan>
                                    </text>
                                  </g>
                                </g>
                                <g id="hero" mask="url(#mask-6)">
                                  <image id="sustainia_bg" x="0" y="0" width="568" height="397"
                                         xlinkHref={image}></image>
                                  <text id="title" font-family="NeueHaasUnicaPro-Black, Neue Haas Unica Pro" font-size="43"
                                        font-weight="700" line-spacing="48" letter-spacing="0.220512822" fill="#FFFFFF">
                                    <tspan x="165.209333" y="314.854335">3D-Printed Shoe Made from</tspan>
                                    <tspan x="244.864628" y="362.854335">Ocean Plastic Waste</tspan>
                                  </text>
                                  <g id="category" transform="translate(458.960000, 221.840000)">
                                    <rect id="background" stroke="#FFFFFF" stroke-width="1.2" x="0.64" y="0.76" width="65.8"
                                          height="21.8"></rect>
                                    <text font-family="SanFranciscoDisplay-Bold, San Francisco Display" font-size="11"
                                          font-weight="bold" line-spacing="12" letter-spacing="0.5" fill="#FFFFFF">
                                      <tspan x="10" y="14.333333">FASHIO</tspan>
                                      <tspan x="49.7167969" y="14.333333">N</tspan>
                                    </text>
                                  </g>
                                </g>
                                <g id="menu/header/load" mask="url(#mask-6)">
                                  <g transform="translate(16.000000, 0.000000)">
                                    <g id="menu-items" transform="translate(443.112157, 0.000000)">
                                      <text id="menu-item" font-family="SanFranciscoDisplay-Regular, San Francisco Display"
                                            font-size="11" font-weight="normal" line-spacing="11" letter-spacing="0.5"
                                            fill="#DDDDDD">
                                        <tspan x="94.72" y="37.0799997">MA</tspan>
                                      </text>
                                      <text id="menu-item" font-family="SanFranciscoDisplay-Bold, San Francisco Display"
                                            font-size="11" font-weight="bold" line-spacing="11" letter-spacing="0.5"
                                            fill="#FFFFFF">
                                        <tspan x="4.72" y="37.0799997">SOLUTION</tspan>
                                        <tspan x="59.1277148" y="37.0799997">S</tspan>
                                      </text>
                                      <polygon id="bar" fill="#FFFFFF"
                                               points="0.647843137 0 69.5043137 0 69.5043137 3.33176471 0.647843137 3.33176471"></polygon>
                                    </g>
                                    <g id="un-logo" transform="translate(0.160000, 13.013333)">
                                      <path
                                        d="M57.75392,9.11463467 C55.057856,9.11463467 53.39264,10.9913067 53.39264,14.0574187 C53.39264,17.0706667 54.925696,18.8944747 57.331008,18.8944747 C58.626176,18.8944747 59.67024,18.3790507 60.172448,17.5596587 L60.19888,17.5596587 L60.19888,18.7226667 L61.560128,18.7226667 L61.560128,13.6345067 L57.674624,13.6345067 L57.674624,14.9561067 L60.079936,14.9561067 C60.06672,16.7799147 59.128384,17.6125227 57.68784,17.6125227 C56.049056,17.6125227 55.031424,16.2644907 55.031424,14.0574187 C55.031424,11.8635627 56.075488,10.5287467 57.75392,10.5287467 C58.956576,10.5287467 59.709888,11.1234667 59.894912,12.1939627 L61.58656,12.1939627 C61.401536,10.3172907 59.987424,9.11463467 57.75392,9.11463467 Z M65.51832,17.3482027 L65.51832,9.31287467 L63.9324,9.31287467 L63.9324,18.7226667 L70.14392,18.7226667 L70.14392,17.3482027 L65.51832,17.3482027 Z M74.908288,9.11463467 C77.604352,9.11463467 79.269568,10.9913067 79.269568,14.0177707 C79.269568,17.0442347 77.604352,18.9209067 74.908288,18.9209067 C72.212224,18.9209067 70.547008,17.0442347 70.547008,14.0177707 C70.547008,10.9913067 72.212224,9.11463467 74.908288,9.11463467 Z M74.908288,10.5287467 C73.229856,10.5287467 72.185792,11.8635627 72.185792,14.0177707 C72.185792,16.1719787 73.229856,17.5067947 74.908288,17.5067947 C76.58672,17.5067947 77.630784,16.1719787 77.630784,14.0177707 C77.630784,11.8635627 76.58672,10.5287467 74.908288,10.5287467 Z M81.126416,9.31287467 L84.364336,9.31287467 C86.611056,9.31287467 87.65512,10.2908587 87.65512,11.7181867 C87.65512,12.7886827 87.113264,13.5155627 86.055984,13.9120427 L86.055984,13.9384747 C87.25864,14.2160107 87.866576,14.9561067 87.866576,16.1455467 C87.866576,17.7182507 86.677136,18.7226667 84.628656,18.7226667 L81.126416,18.7226667 L81.126416,9.31287467 Z M82.712336,10.6212587 L82.712336,13.3041067 L84.271824,13.3041067 C85.580208,13.3041067 86.122064,12.8283307 86.122064,11.9560747 C86.122064,11.0706027 85.593424,10.6212587 84.35112,10.6212587 L82.712336,10.6212587 Z M82.712336,14.5728427 L82.712336,17.4010667 L84.390768,17.4010667 C85.685936,17.4010667 86.254224,16.9385067 86.254224,16.0001707 C86.254224,15.0750507 85.699152,14.5728427 84.28504,14.5728427 L82.712336,14.5728427 Z M94.785152,16.1719787 L90.886432,16.1719787 L90.014176,18.7226667 L88.34896,18.7226667 L91.771904,9.31287467 L93.89968,9.31287467 L97.322624,18.7226667 L95.657408,18.7226667 L94.785152,16.1719787 Z M94.309376,14.7975147 L94.058272,14.0574187 C93.63536,12.8018987 93.225664,11.5728107 92.8424,10.3172907 L92.815968,10.3172907 C92.432704,11.5728107 92.04944,12.8018987 91.613312,14.0574187 L91.362208,14.7975147 L94.309376,14.7975147 Z M100.025296,17.3482027 L100.025296,9.31287467 L98.439376,9.31287467 L98.439376,18.7226667 L104.650896,18.7226667 L104.650896,17.3482027 L100.025296,17.3482027 Z M113.043056,9.11463467 C115.73912,9.11463467 117.404336,10.9913067 117.404336,14.0177707 C117.404336,17.0442347 115.73912,18.9209067 113.043056,18.9209067 C110.346992,18.9209067 108.681776,17.0442347 108.681776,14.0177707 C108.681776,10.9913067 110.346992,9.11463467 113.043056,9.11463467 Z M113.043056,10.5287467 C111.364624,10.5287467 110.32056,11.8635627 110.32056,14.0177707 C110.32056,16.1719787 111.364624,17.5067947 113.043056,17.5067947 C114.721488,17.5067947 115.765552,16.1719787 115.765552,14.0177707 C115.765552,11.8635627 114.721488,10.5287467 113.043056,10.5287467 Z M119.261184,9.31287467 L122.657696,9.31287467 C124.877984,9.31287467 126.027776,10.4890987 126.027776,12.2468267 C126.027776,14.0177707 124.877984,15.1939947 122.657696,15.1939947 L120.847104,15.1939947 L120.847104,18.7226667 L119.261184,18.7226667 L119.261184,9.31287467 Z M120.847104,10.6476907 L120.847104,13.8591787 L122.472672,13.8591787 C123.807488,13.8591787 124.402208,13.2512427 124.402208,12.2468267 C124.402208,11.2556267 123.807488,10.6476907 122.472672,10.6476907 L120.847104,10.6476907 Z M127.6996,9.31287467 L131.096112,9.31287467 C133.3164,9.31287467 134.466192,10.4890987 134.466192,12.2468267 C134.466192,14.0177707 133.3164,15.1939947 131.096112,15.1939947 L129.28552,15.1939947 L129.28552,18.7226667 L127.6996,18.7226667 L127.6996,9.31287467 Z M129.28552,10.6476907 L129.28552,13.8591787 L130.911088,13.8591787 C132.245904,13.8591787 132.840624,13.2512427 132.840624,12.2468267 C132.840624,11.2556267 132.245904,10.6476907 130.911088,10.6476907 L129.28552,10.6476907 Z M139.997088,9.11463467 C142.693152,9.11463467 144.358368,10.9913067 144.358368,14.0177707 C144.358368,17.0442347 142.693152,18.9209067 139.997088,18.9209067 C137.301024,18.9209067 135.635808,17.0442347 135.635808,14.0177707 C135.635808,10.9913067 137.301024,9.11463467 139.997088,9.11463467 Z M139.997088,10.5287467 C138.318656,10.5287467 137.274592,11.8635627 137.274592,14.0177707 C137.274592,16.1719787 138.318656,17.5067947 139.997088,17.5067947 C141.67552,17.5067947 142.719584,16.1719787 142.719584,14.0177707 C142.719584,11.8635627 141.67552,10.5287467 139.997088,10.5287467 Z M146.215216,9.31287467 L149.453136,9.31287467 C151.765936,9.31287467 152.81,10.2776427 152.81,11.9164267 C152.81,13.0926507 152.135984,13.9781227 151.065488,14.2424427 L151.065488,14.2688747 C151.739504,14.4935467 152.043472,14.8900267 152.466384,16.0662507 L153.431152,18.7226667 L151.699856,18.7226667 L150.906896,16.3570027 C150.536848,15.2997227 150.127152,14.9428907 149.162384,14.9428907 L147.801136,14.9428907 L147.801136,18.7226667 L146.215216,18.7226667 L146.215216,9.31287467 Z M147.801136,10.6212587 L147.801136,13.6477227 L149.37384,13.6477227 C150.682224,13.6477227 151.22408,13.0133547 151.210864,12.0750187 C151.210864,11.0706027 150.523632,10.6212587 149.281328,10.6212587 L147.801136,10.6212587 Z M161.340928,10.6873387 L161.340928,9.31287467 L153.583136,9.31287467 L153.583136,10.6873387 L156.67568,10.6873387 L156.67568,18.7226667 L158.248384,18.7226667 L158.248384,10.6873387 L161.340928,10.6873387 Z M169.739696,9.31287467 L168.166992,9.31287467 L168.166992,15.1146987 C168.166992,16.7006187 167.426896,17.5067947 166.131728,17.5067947 C164.83656,17.5067947 164.096464,16.7006187 164.096464,15.1146987 L164.096464,9.31287467 L162.510544,9.31287467 L162.510544,15.1675627 C162.510544,17.6125227 163.805712,18.9209067 166.131728,18.9209067 C168.457744,18.9209067 169.739696,17.6125227 169.739696,15.1675627 L169.739696,9.31287467 Z M179.949056,18.7226667 L179.949056,9.31287467 L178.429216,9.31287467 L178.429216,13.0133547 C178.429216,14.4406827 178.455648,15.8680107 178.495296,17.2953387 L178.468864,17.2953387 C177.834496,15.9473067 177.16048,14.5860587 176.473248,13.2512427 L174.4512,9.31287467 L172.164832,9.31287467 L172.164832,18.7226667 L173.697888,18.7226667 L173.697888,15.0221867 C173.697888,13.5948587 173.671456,12.1675307 173.618592,10.7402027 L173.645024,10.7402027 C174.279392,12.0882347 174.953408,13.4494827 175.64064,14.7842987 L177.675904,18.7226667 L179.949056,18.7226667 Z M184.039408,18.7226667 L184.039408,9.31287467 L182.453488,9.31287467 L182.453488,18.7226667 L184.039408,18.7226667 Z M193.046112,10.6873387 L193.046112,9.31287467 L185.28832,9.31287467 L185.28832,10.6873387 L188.380864,10.6873387 L188.380864,18.7226667 L189.953568,18.7226667 L189.953568,10.6873387 L193.046112,10.6873387 Z M198.27304,14.9428907 L201.563824,9.31287467 L199.779664,9.31287467 L198.762032,11.1763307 C198.312688,11.9692907 197.889776,12.7886827 197.493296,13.6080747 L197.466864,13.6080747 C197.070384,12.7886827 196.647472,11.9692907 196.211344,11.1763307 L195.180496,9.31287467 L193.38312,9.31287467 L196.68712,14.9428907 L196.68712,18.7226667 L198.27304,18.7226667 L198.27304,14.9428907 Z M60.582144,26.3615147 L60.582144,24.1808747 L53.775904,24.1808747 L53.775904,33.5906667 L60.66144,33.5906667 L60.66144,31.4100267 L56.419104,31.4100267 L56.419104,29.9034027 L60.291392,29.9034027 L60.291392,27.7756267 L56.419104,27.7756267 L56.419104,26.3615147 L60.582144,26.3615147 Z M67.883984,28.7139627 L70.593264,24.1808747 L67.553584,24.1808747 L66.11304,27.2073387 L66.086608,27.2073387 L64.65928,24.1808747 L61.6196,24.1808747 L64.32888,28.7139627 L61.42136,33.5906667 L64.540336,33.5906667 L66.086608,30.4320427 L66.11304,30.4320427 L67.672528,33.5906667 L70.778288,33.5906667 L67.883984,28.7139627 Z M71.63072,24.1808747 L75.608736,24.1808747 C78.132992,24.1808747 79.348864,25.4363947 79.348864,27.3923627 C79.348864,29.3483307 78.132992,30.6170667 75.608736,30.6170667 L74.27392,30.6170667 L74.27392,33.5906667 L71.63072,33.5906667 L71.63072,24.1808747 Z M74.27392,26.2557867 L74.27392,28.5421547 L75.238688,28.5421547 C76.229888,28.5421547 76.626368,28.1060267 76.626368,27.3923627 C76.626368,26.6919147 76.229888,26.2557867 75.238688,26.2557867 L74.27392,26.2557867 Z M83.333488,31.3703787 L83.333488,24.1808747 L80.690288,24.1808747 L80.690288,33.5906667 L87.417232,33.5906667 L87.417232,31.3703787 L83.333488,31.3703787 Z M92.485568,23.9694187 C95.340224,23.9694187 97.150816,25.8725227 97.150816,28.8857707 C97.150816,31.8990187 95.340224,33.8021227 92.485568,33.8021227 C89.644128,33.8021227 87.833536,31.8990187 87.833536,28.8857707 C87.833536,25.8725227 89.644128,23.9694187 92.485568,23.9694187 Z M92.485568,26.2161387 C91.282912,26.2161387 90.608896,27.1809067 90.608896,28.8857707 C90.608896,30.5906347 91.282912,31.5554027 92.485568,31.5554027 C93.70144,31.5554027 94.375456,30.5906347 94.375456,28.8857707 C94.375456,27.1809067 93.70144,26.2161387 92.485568,26.2161387 Z M98.637616,24.1808747 L102.827088,24.1808747 C105.258832,24.1808747 106.316112,25.1720747 106.316112,26.8769387 C106.316112,28.0399467 105.655312,28.8461227 104.558384,29.1765227 L104.558384,29.2029547 C105.245616,29.4540587 105.589232,29.9298347 106.051792,31.1589227 L106.924048,33.5906667 L104.122256,33.5906667 L103.44824,31.5686187 C103.131056,30.6567147 102.774224,30.3263147 101.941616,30.3263147 L101.280816,30.3263147 L101.280816,33.5906667 L98.637616,33.5906667 L98.637616,24.1808747 Z M101.280816,26.1632747 L101.280816,28.4099947 L102.404176,28.4099947 C103.368944,28.4099947 103.738992,27.9606507 103.738992,27.2469867 C103.738992,26.4804587 103.236784,26.1632747 102.338096,26.1632747 L101.280816,26.1632747 Z M114.926336,26.3615147 L114.926336,24.1808747 L108.120096,24.1808747 L108.120096,33.5906667 L115.005632,33.5906667 L115.005632,31.4100267 L110.763296,31.4100267 L110.763296,29.9034027 L114.635584,29.9034027 L114.635584,27.7756267 L110.763296,27.7756267 L110.763296,26.3615147 L114.926336,26.3615147 Z M116.677456,24.1808747 L120.866928,24.1808747 C123.298672,24.1808747 124.355952,25.1720747 124.355952,26.8769387 C124.355952,28.0399467 123.695152,28.8461227 122.598224,29.1765227 L122.598224,29.2029547 C123.285456,29.4540587 123.629072,29.9298347 124.091632,31.1589227 L124.963888,33.5906667 L122.162096,33.5906667 L121.48808,31.5686187 C121.170896,30.6567147 120.814064,30.3263147 119.981456,30.3263147 L119.320656,30.3263147 L119.320656,33.5906667 L116.677456,33.5906667 L116.677456,24.1808747 Z M119.320656,26.1632747 L119.320656,28.4099947 L120.444016,28.4099947 C121.408784,28.4099947 121.778832,27.9606507 121.778832,27.2469867 C121.778832,26.4804587 121.276624,26.1632747 120.377936,26.1632747 L119.320656,26.1632747 Z"
                                        id="text" fill="#FFFFFF"></path>
                                      <g id="circle" opacity="0.7" transform="translate(0.000000, 0.550667)"
                                         fill-rule="nonzero" fill="#FFFFFF">
                                        <path
                                          d="M11.0452878,15.354145 C11.5779199,14.445939 12.2266385,13.616262 12.9675434,12.8821857 L6.66473005,5.96821113 C5.08049094,7.48416407 3.72501049,9.24253291 2.66316057,11.1784458 L11.0452878,15.354145 Z"
                                          id="Shape"></path>
                                        <path
                                          d="M26.4984477,10.4716839 C27.4544541,10.9087154 28.3421742,11.4652477 29.1411224,12.1276235 L35.4644216,5.22389189 C33.7982391,3.78646804 31.9101266,2.60170302 29.8615415,1.72764006 L26.4984477,10.4716839 Z"
                                          id="Shape"></path>
                                        <path
                                          d="M40.9751154,12.5509978 L32.5964025,16.726697 C32.9958766,17.6588032 33.2826785,18.6523669 33.4363224,19.6869024 L42.7573844,18.8060108 C42.4808254,16.6003676 41.8730785,14.4971536 40.9751154,12.5509978"
                                          id="Shape"></path>
                                        <path
                                          d="M31.9954843,15.5146175 L40.3741971,11.3389183 C39.3362474,9.40983402 38.0114957,7.6582938 36.4579854,6.14234086 L30.1381005,13.0426582 C30.8585196,13.7835631 31.483338,14.6132401 31.9954843,15.5146175"
                                          id="Shape"></path>
                                        <path
                                          d="M9.37910525,21.4691714 C9.37910525,21.2813844 9.38593386,21.0935975 9.39276248,20.9058105 L0.0717004769,20.0727192 C0.0409717011,20.5336509 0.023900159,21.0014111 0.023900159,21.4725857 C0.023900159,23.2616833 0.245830207,24.996152 0.658961526,26.6589202 L9.66249285,24.0708744 C9.47812019,23.2343688 9.37910525,22.3637202 9.37910525,21.4691714"
                                          id="Shape"></path>
                                        <path
                                          d="M30.8243765,29.1308795 C30.1620006,29.9366563 29.3971955,30.6570754 28.5504471,31.2682366 L33.4738798,39.2372324 C35.3005348,37.9978385 36.9257456,36.4887141 38.2948833,34.7610741 L30.8243765,29.1308795 Z"
                                          id="Shape"></path>
                                        <path
                                          d="M33.5660661,21.4691714 C33.5660661,22.350063 33.4704655,23.213883 33.2895072,24.0401456 L42.2930385,26.6281914 C42.7027555,24.9756661 42.9212712,23.2480261 42.9212712,21.4691714 C42.9212712,21.0287256 42.907614,20.5882798 42.8802995,20.1512483 L33.5592375,21.0321399 C33.5626518,21.1789552 33.5660661,21.3257704 33.5660661,21.4691714"
                                          id="Shape"></path>
                                        <path
                                          d="M12.2744388,29.3118378 L4.82441781,34.9761755 C6.21062703,36.6833297 7.85290938,38.1685539 9.68980731,39.3840477 L14.6098258,31.4218804 C13.7425914,30.8209622 12.9573005,30.110786 12.2744388,29.3118378"
                                          id="Shape"></path>
                                        <path
                                          d="M9.53274913,19.560573 C9.70005024,18.508966 10.0039237,17.501745 10.4272979,16.5559816 L2.04858506,12.3836967 C1.12330747,14.3537526 0.488246105,16.4842811 0.201444197,18.7240674 L9.53274913,19.560573 Z"
                                          id="Shape"></path>
                                        <path
                                          d="M32.3266722,39.9610658 L27.410068,32.0023129 C26.5155192,32.5076305 25.5560986,32.9036903 24.542049,33.1665921 L26.2765176,42.3749819 C28.4377749,41.8799072 30.4727027,41.0570588 32.3266722,39.9610658"
                                          id="Shape"></path>
                                        <path
                                          d="M32.9310048,25.3444114 C32.6032312,26.3004178 32.1627854,27.2052095 31.6233246,28.0383008 L39.0972458,33.675324 C40.3127396,31.9306124 41.2721603,29.9981138 41.9277075,27.9290429 L32.9310048,25.3444114 Z"
                                          id="Shape"></path>
                                        <path
                                          d="M23.2172973,33.4363224 C22.6471078,33.5182658 22.0666754,33.5626518 21.4725857,33.5626518 C20.9979968,33.5626518 20.5268223,33.5319231 20.0658906,33.4807084 L18.3314219,42.6890983 C19.3591288,42.8393278 20.4073215,42.9178569 21.4725857,42.9178569 C22.6573507,42.9178569 23.8182156,42.818842 24.9483517,42.6344693 L23.2172973,33.4363224 Z"
                                          id="Shape"></path>
                                        <path
                                          d="M22.2476337,9.3995911 C23.2855835,9.46787727 24.2893901,9.66249285 25.2385679,9.97660922 L28.6016617,1.23939396 C26.6008769,0.532632114 24.4669342,0.116086486 22.2476337,0.0375573927 L22.2476337,9.3995911 Z"
                                          id="Shape"></path>
                                        <path
                                          d="M18.7343103,33.2519498 C17.6861176,33.0061196 16.6891396,32.6271313 15.7672763,32.1286423 L10.8438436,40.0942238 C12.7387847,41.1799739 14.81127,41.9823364 17.0066703,42.4500967 L18.7343103,33.2519498 Z"
                                          id="Shape"></path>
                                        <path
                                          d="M17.8397615,9.93563752 C18.8094251,9.62834976 19.8337176,9.4405628 20.8955676,9.39276248 L20.8955676,0.0307287758 C18.6455383,0.0921863275 16.4842811,0.501903339 14.4561819,1.20525087 L17.8397615,9.93563752 Z"
                                          id="Shape"></path>
                                        <path
                                          d="M11.4550048,28.2363307 C10.8609151,27.3588534 10.3760833,26.3994328 10.0278238,25.3751402 L1.03112114,27.9563574 C1.71398283,30.1039574 2.72120382,32.1013278 4.00156948,33.8972541 L11.4550048,28.2363307 Z"
                                          id="Shape"></path>
                                        <path
                                          d="M13.981593,11.9842226 C14.7737126,11.3594041 15.6409469,10.8301863 16.5764674,10.4136407 L13.1928878,1.68666836 C11.1784458,2.53341685 9.321062,3.67721017 7.67536534,5.0668337 L13.981593,11.9842226 Z"
                                          id="Shape"></path>
                                      </g>
                                    </g>
                                  </g>
                                </g>
                              </g>
                              <rect id="screen" fill="url(#linearGradient-10b)" x="34.9268326" y="46.6994413" width="568"
                                    height="616"></rect>
                              <g id="camera" opacity="0.5">
                                <use fill="url(#radialGradient-11)" fill-rule="evenodd" xlinkHref="#path--12"></use>
                                <ellipse stroke="#1A1C1E" stroke-width="1.3" cx="526.425759" cy="26.1994413" rx="3.14892612"
                                         ry="3.15"></ellipse>
                              </g>
                            </g>
                            <g id="cabinet" transform="translate(0.000000, 718.000000)">
                              <path d="M0,7 C37.3333333,19.6666667 103.666667,26 199,26 L776,26 L776,7 L0,7 Z" id="bottom"
                                    fill="url(#linearGradient-13b)"></path>
                              <ellipse id="screw-2" fill="url(#radialGradient-14)" cx="500.2" cy="19.3005587" rx="5.2"
                                       ry="1.30055866"></ellipse>
                              <ellipse id="screw-1" fill="url(#radialGradient-14)" cx="134.2" cy="19.3005587" rx="5.2"
                                       ry="1.30055866"></ellipse>
                              <g id="frame">
                                <use fill="url(#linearGradient-15b)" fill-rule="evenodd" xlinkHref="#path--16"></use>
                                <use fill="black" fill-opacity="1" filter="url(#filter--17)" xlinkHref="#path--16"></use>
                              </g>
                              <g id="handle">
                                <use fill="url(#linearGradient-18b)" fill-rule="evenodd" xlinkHref="#path--19"></use>
                                <use fill="black" fill-opacity="1" filter="url(#filter--20)" xlinkHref="#path--19"></use>
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
                    </div>
                  )}

                  {device === "ipad" && (
                    <div className="ipad-padding-hack">
                      <svg className="case__svg" viewBox="0 0 608 696" version="1.1" xmlns="http://www.w3.org/2000/svg"
                           xmlnsXlink="http://www.w3.org/1999/xlink">
                        <title>ipad</title>
                        <defs>
                          <rect id="path---1" x="0" y="0" width="608" height="696"></rect>
                          <linearGradient x1="0%" y1="0%" x2="101.069315%" y2="100%" id="linearGradient---3">
                            <stop stop-color="#FAFCFE" offset="0%"></stop>
                            <stop stop-color="#F2F4F6" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="0%" y1="0%" x2="100%" y2="100%" id="linearGradient---4">
                            <stop stop-color="#DADCDE" offset="0%"></stop>
                            <stop stop-color="#BABCBE" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="0%" y1="0%" x2="100%" y2="100%" id="linearGradient---5">
                            <stop stop-color="#EAECEE" offset="0%"></stop>
                            <stop stop-color="#DADCDE" offset="100%"></stop>
                          </linearGradient>
                          <path
                            d="M-130,6 L570,6 L570,6 C587.673112,6 602,20.326888 602,38 L602,658 L602,658 C602,675.673112 587.673112,690 570,690 L-130,690 L-130,6 Z"
                            id="path---6"></path>
                          <filter x="-1.3%" y="-1.4%" width="102.6%" height="102.8%" filterUnits="objectBoundingBox"
                                  id="filter---7">
                            <feGaussianBlur stdDeviation="2.5" in="SourceAlpha" result="shadowBlurInner1"></feGaussianBlur>
                            <feOffset dx="0" dy="2" in="shadowBlurInner1" result="shadowOffsetInner1"></feOffset>
                            <feComposite in="shadowOffsetInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1"
                                         result="shadowInnerInner1"></feComposite>
                            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 1 0" type="matrix"
                                           in="shadowInnerInner1" result="shadowMatrixInner1"></feColorMatrix>
                            <feGaussianBlur stdDeviation="2" in="SourceAlpha" result="shadowBlurInner2"></feGaussianBlur>
                            <feOffset dx="0" dy="0" in="shadowBlurInner2" result="shadowOffsetInner2"></feOffset>
                            <feComposite in="shadowOffsetInner2" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1"
                                         result="shadowInnerInner2"></feComposite>
                            <feColorMatrix
                              values="0 0 0 0 0.0392156863   0 0 0 0 0.0470588235   0 0 0 0 0.0549019608  0 0 0 0.25 0"
                              type="matrix" in="shadowInnerInner2" result="shadowMatrixInner2"></feColorMatrix>
                            <feMerge>
                              <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                              <feMergeNode in="shadowMatrixInner2"></feMergeNode>
                            </feMerge>
                          </filter>
                          <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient---8">
                            <stop stop-color="#E8EAEC" offset="0%"></stop>
                            <stop stop-color="#F4F6F8" offset="49.798943%"></stop>
                            <stop stop-color="#F6F8FA" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="83.9576666%" y1="1.96998351%" x2="14.9512656%" y2="96.0345747%"
                                          id="linearGradient---9">
                            <stop stop-color="#BABCBE" offset="0%"></stop>
                            <stop stop-color="#CACCCE" offset="26.7759563%"></stop>
                            <stop stop-color="#EAECEE" offset="51.5030243%"></stop>
                            <stop stop-color="#CACCCE" offset="85.1595538%"></stop>
                            <stop stop-color="#9A9C9E" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient---10">
                            <stop stop-color="#DADCDE" offset="0%"></stop>
                            <stop stop-color="#FFFFFF" offset="100%"></stop>
                          </linearGradient>
                          <circle id="path---11" cx="569" cy="348" r="18"></circle>
                          <linearGradient x1="0%" y1="0%" x2="129.076503%" y2="84.0944371%" id="linearGradient---12">
                            <stop stop-color="#FFFFFF" stop-opacity="0.7" offset="0%"></stop>
                            <stop stop-color="#000000" stop-opacity="0.1" offset="100%"></stop>
                          </linearGradient>
                          <linearGradient x1="0%" y1="0%" x2="100%" y2="100%" id="linearGradient---13">
                            <stop stop-color="#555555" offset="0%"></stop>
                            <stop stop-color="#000000" offset="100%"></stop>
                          </linearGradient>
                          <rect id="path---14" x="0" y="28" width="534" height="640"></rect>
                        </defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g id="ipad">
                            <mask id="mask---2" fill="white">
                              <use xlinkHref="#path---1"></use>
                            </mask>
                            <g id="size"></g>
                            <g id="glass" mask="url(#mask---2)">
                              <use fill="url(#linearGradient---3)" fill-rule="evenodd" xlinkHref="#path---6"></use>
                              <use fill="black" fill-opacity="1" filter="url(#filter---7)" xlinkHref="#path---6"></use>
                              <path stroke="url(#linearGradient---4)" stroke-width="6"
                                    d="M-133,3 L570,3 C589.329966,3 605,18.6700338 605,38 L605,658 C605,677.329966 589.329966,693 570,693 L-133,693 L-133,3 Z"></path>
                              <path stroke="url(#linearGradient---5)" stroke-width="3"
                                    d="M-131.5,4.5 L570,4.5 C588.501539,4.5 603.5,19.4984609 603.5,38 L603.5,658 C603.5,676.501539 588.501539,691.5 570,691.5 L-131.5,691.5 L-131.5,4.5 Z"></path>
                            </g>
                            <g id="button" mask="url(#mask---2)">
                              <use fill="url(#linearGradient---8)" fill-rule="evenodd" xlinkHref="#path---11"></use>
                              <circle stroke="url(#linearGradient---9)" stroke-width="3" cx="569" cy="348"
                                      r="19.5"></circle>
                              <circle stroke="url(#linearGradient---10)" stroke-width="1" cx="569" cy="348"
                                      r="17.5"></circle>
                            </g>
                            <image id="packinglist" mask="url(#mask---2)" x="0" y="28" width="534" height="640"
                                   xlinkHref={image}></image>
                            <g id="screen" stroke-linejoin="round" mask="url(#mask---2)">
                              <use fill="url(#linearGradient---12)" fill-rule="evenodd" xlinkHref="#path---14"></use>
                              <rect stroke="url(#linearGradient---13)" stroke-width="4" x="-2" y="26" width="538"
                                    height="644"></rect>
                            </g>
                          </g>
                        </g>
                      </svg>
                    </div>
                  )}

                </div>
              </div>
            </div>
            <div className="flex-item-50 full-height">
              <div className="flex-align">
                <div className="case__details">
                  <h2 className="case__title font-heading letter-spacing-m fluid-typo-l">{title}</h2>
                  <p className="case__client letter-spacing-s fluid-typo-xs weight-700">{subtitle}</p>
                  <p className="case__description letter-spacing-xs fluid-typo-s weight-300">{description}</p>
                  <h3 className="case__stack-title letter-spacing-s fluid-typo-xs weight-700 uppercase">SKARP's
                    contribution</h3>
                  <p className="case__stack letter-spacing-xs fluid-typo-s weight-300">{stack}</p>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    )}

  </section>
)

const CasesPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <Layout>
      <ProductPageTemplate
        {...frontmatter}
      />
    </Layout>
  )
}

CasesPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default CasesPage

export const casesPageQuery = graphql`
  query CasesPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        cases {
          title
          subtitle
          description
          stack
          device
          image
        }
      }
    }
  }
`
