import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql } from 'gatsby'

import Layout from '../components/Layout'
import { HTMLContent } from '../components/Content'

export const IndexPageTemplate = ({
  firstPitch,
  secondPitch,
  tagline,
  devStack,
  background
}) => (
  <div style={{ background: background }}>
    <section id="about" className="section panel panel--200 panel--about">
      <div className="panel">
        <div>
          <div className="panel z-1 bg-white">
            <div className="flex-grid">
              <div className="flex-row flex-row--center">
                <div className="flex-item-50">
                  <HTMLContent
                    className="fluid-typo-xl letter-spacing-l dark font-heading about-text"
                    content={firstPitch.childMarkdownRemark.html} />
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div className="panel">
        <div className="spacer">
          <div className="panel z-1">
            <div className="flex-grid">
              <div className="flex-row flex-row--center">
                <div className="flex-item-50">
                  <HTMLContent
                    className="fluid-typo-xl letter-spacing-l light font-heading about-text"
                    content={secondPitch.childMarkdownRemark.html} />
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div className="stack z-3">
        <div id="stack" className="stack-content fluid-typo-tiny">
          <div className="stack-header">
            <svg viewBox="0 0 672 38">
              <path
                d="M0,36 L672,36 L672,38 L0,38 L0,36 Z M66,24 L664,24 L664,26 L66,26 L66,24 Z M66,18 L641,18 L664,18 L664,20 L642,20 L66,20 L66,18 Z M66,12 L664,12 L664,14 L66,14 L66,12 Z M66,6 L664,6 L664,8 L66,8 L66,6 Z M66,0 L664,0 L664,2 L66,2 L66,0 Z M32,0 L58,0 L58,26 L32,26 L32,0 Z M34,2 L34,24 L56,24 L56,2 L34,2 Z M8,24 L24,24 L24,26 L8,26 L8,24 Z M8,18 L24,18 L24,20 L8,20 L8,18 Z M8,12 L24,12 L24,14 L8,14 L8,12 Z M8,6 L24,6 L24,8 L8,8 L8,6 Z M8,0 L24,0 L24,2 L8,2 L8,0 Z"></path>
            </svg>
            <h1>
              <pre className="pre">{devStack.heading}</pre>
            </h1>
          </div>
          <div className="stack-body">
            <pre className="pre">
              {`<html>
    <head>
      <title>`}<span className="white">{devStack.description}</span>{`</title>
    </head>

    <body>`}<br />
              {devStack.stacks.map(stack => (<>
                  {`    <ul id="`}<span className="blue-highlight">{stack.title}</span>{`">`}<br />
                    {stack.technologies.map(technology => (<>
                      {`        <li> `}<span className="white">{technology.title}</span>{` </li>`}<br />
                    </>))}
                  {`    </ul>`}<br />
                </>
              ))}
              {`    </body>
</html>`}
            </pre>
          </div>
        </div>
      </div>

      <div id="cta" className="panel panel--50 panel--cta flex-align z-2">
        <div className="cta">
          <p className="cta__text font-heading letter-spacing-m fluid-typo-l light weight-600">{tagline}</p>
          <a className="cta__button button fluid-typo-m weight-300 color-blue" href="#contact">The coffee is on us!</a>
        </div>
      </div>
    </section>
  </div>
)

IndexPageTemplate.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  title: PropTypes.string,
  heading: PropTypes.string,
  subheading: PropTypes.string,
  mainpitch: PropTypes.object,
  description: PropTypes.string,
  intro: PropTypes.shape({
    blurbs: PropTypes.array,
  }),
}

const IndexPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <Layout>
      <IndexPageTemplate {...frontmatter} />
    </Layout>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexPageTemplate {
    markdownRemark(frontmatter: { templateKey: { eq: "index-page" } }) {
      html
      frontmatter {
        background
        firstPitch {
            childMarkdownRemark {
                html
            }
        }
        secondPitch {
            childMarkdownRemark {
                html
            }
        }
        tagline
        devStack {
            heading
            description
            stacks {
                title
                technologies {
                    title
                }
                comment
            }
        }
      }
    }
  }
`
